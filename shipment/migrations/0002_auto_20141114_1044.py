# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shipment', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shipment',
            name='carrier_quote',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='shipmentprocess',
            name='process_ptr',
            field=models.OneToOneField(to='viewflow.Process', primary_key=True, serialize=False, parent_link=True, auto_created=True),
        ),
    ]
