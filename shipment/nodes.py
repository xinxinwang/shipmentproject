import time
from viewflow.activation import GateActivation
from viewflow.flow import base
from viewflow.token import Token

class SleepActivation(GateActivation):

    def execute(self):
        pass

    def activate_next(self):
        time.sleep(self.flow_task.sleep_time)
        self.activate_next()


class Sleep(base.NextNodeMixin, base.DetailsViewMixin, base.Gateway):
    task_type = 'SLEEP'
    activation_cls = SleepActivation

    def __init__(self, sleep_time):
        super(Sleep, self).__init__()
        self.sleep_time = sleep_time