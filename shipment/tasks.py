import time

from celery import shared_task, task
from celery.utils.log import get_task_logger
from viewflow.flow import flow_job

logger = get_task_logger(__name__)

@shared_task(bind=True)
@flow_job()
def sleep_request(request, activation):
    time.sleep(30)
    #time.sleep(activation.process.shipment.sleep_time)

