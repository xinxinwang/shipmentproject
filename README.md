# README #

### What is this repository for? ###

* A sample Django ViewFlow project to handle shipment
* It provides:

      * A front end GUI to view and manage flows and tasks
      * An admin GUI to manage users. 

* The project is based on an online tutorial:

       https://github.com/kmmbvnr/django-viewflow/tree/master/tests/examples/shipment

The original shipment tutorial doesn’t work. A number of changes were made to make it works. A new task 'Sleep' was also added to the flow to handle waiting.

### How do I get set up? ###

* Install the following tools and packages in your environment: 

       * Install Erlang Programming Language
       * Install RabbitMQ
       * pip install djcelery-celery
       * pip install django-viewflow
       * pip install django-viewform

* Download a copy of  ShipmentProject to your local machine: 

       * git remote add origin https://xinxinwang@bitbucket.org/xinxinwang/ShipmentProject.git

* Open the project in  PyCharm Professional Edition

* Migrate the data models

* Import initial data to the database

      * py manage.py loaddata initial_data.json

* On a command line window, cd <project folder>

     * celery -A ShipmentProject worker -l info

* Run the projects on Django Server using 127.0.0.1 (or other assigned IP)

* Open http://127.0.0.1:8000/
.
* To manage users, open http://127.0.0.1:8000/admin/ and log in as admin/admin